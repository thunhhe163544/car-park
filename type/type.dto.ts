export type EmployeeType = {
    Id: Number;
    account: string;
    department: string;
    address: string;
    birthdate: Date;
    email: string;
    name: string;
    phone: string;
    password: string;
    sex: string 
};


export type TripType = {

    Id: number;
    bookedTicketNumber: number;
    carType: string;
    departureDate: Date;
    departureTime: string;
    destination: string;
    driver: string;
    maximumOnlineTicketNumber: number

};


export type BookingOffice ={
    id: number;
    endContractDeadline: Date;
    officeName: string;
    officePhone:string;
    officePlace: string;
    officePrice: number;
    startContractDeadline: Date;
    tripId: number;

}


    export type Car = {

        licensePlate: string;
        carColor: string;
        carType: string ;
        company: string ;
        parkingId:   number 

    }
