[GET] api/bookingoffice/add: get data for booking office form
[POST] api/bookingoffice/add: create booking office:
 example data:
 ```js
 {
   "endContractDeadline": "2024-01-11",
    "officeName": "Viet Anh",
    "officePhone":"0399377408",
    "officePlace": "Ha Noi",
    "officePrice": 5000,
    "startContractDeadline": "2024-07-11"
}
```



```js
  {
    "bookedTicketNumber": 5,
    "carType": "Carbin",
    "departureDate": "2024-06-11",
    "departureTime": "12:30:00",
    "destination": "Sapa",
    "driver": "Van Trang",
    "maximumOnlineTicketNumber": 2
}
```
