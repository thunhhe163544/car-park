import React from "react";

function ProductList() {
  return (
    <div className="offset-2 col-md-8">
      <h3>Add Employee</h3>

      <form>
        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputFullName" className="col-form-label">
              Full name
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="text"
              className="form-control"
              id="inputFullName"
              placeholder="Enter full name"
            />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputPhoneNumber" className="col-form-label">
              Phone number
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="text"
              className="form-control"
              id="inputPhoneNumber"
              placeholder="Enter phone number"
            />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputDateOfBirth" className="col-form-label">
              Date of birth
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input type="date" className="form-control" id="inputDateOfBirth" />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputSex" className="col-form-label">
              Sex
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="radio"
              name="sex"
              value="male"
              id="inputMale"
              style={{ margin: "15px" }}
            />
            <label htmlFor="inputMale">Male</label>
            <input
              type="radio"
              name="sex"
              value="female"
              id="inputFemale"
              style={{ margin: "15px" }}
            />
            <label htmlFor="inputFemale">Female</label>
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputAddress" className="col-form-label">
              Address
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="text"
              className="form-control"
              id="inputAddress"
              placeholder="Enter address"
            />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputEmail" className="col-form-label">
              Email
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="text"
              className="form-control"
              id="inputEmail"
              placeholder="Enter email"
            />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputAccount" className="col-form-label">
              Account
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="text"
              className="form-control"
              id="inputAccount"
              placeholder="Enter account"
            />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputPassword" className="col-form-label">
              Password
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <input
              type="text"
              className="form-control"
              id="inputPassword"
              placeholder="Enter Password"
            />
          </div>
        </div>

        <div className="row g-3 align-items-center mb-3">
          <div className="col-md-3">
            <label htmlFor="inputDepartment" className="col-form-label">
              Department
              <span style={{ color: "red" }}> (*)</span>
            </label>
          </div>
          <div className="col-md-9">
            <select
              className="form-select"
              id="inputDepartment"
              aria-label="Default select example"
            >
              <option selected>Employee</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
        </div>

        <div style={{ textAlign: "center" }}>
          <button
            type="button"
            style={{
              backgroundColor: "#007BFF",
              borderColor: "#007BFF",
              color: "white",
              marginRight: "10px",
            }}
          >
            Back
          </button>
          <button
            type="reset"
            style={{
              backgroundColor: "#FFC107",
              borderColor: "#FFC107",
              color: "black",
              marginRight: "10px",
            }}
          >
            Reset
          </button>
          <button
            type="submit"
            style={{
              backgroundColor: "#28A745",
              borderColor: "#28A745",
              color: "white",
            }}
          >
            ADD
          </button>
        </div>
      </form>
    </div>
  );
}

export default ProductList;
