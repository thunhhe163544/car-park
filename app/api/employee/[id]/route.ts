import { NextResponse } from "next/server";

import prisma from "@/prisma";
import { EmployeeType } from "@/type/type.dto";

// export const GET = async (req:Request, res: NextResponse) => {
    

//     try {
//         const id = parseInt(req.url.split("/employee/")[1]);
//         const employee = await prisma.employee.findFirst({where: {id}}); //find all employee in db
//         return NextResponse.json({ message: "Get all employee successful", employees }, { status: 200 });

//     } catch (err) {
//         return NextResponse.json({ message: "Error get all employee", err }, { status: 500 });

// }

export async function GET(req: Request,{ params }: { params: { id: string }}, 
  ) {
    try {
      if (!params.id) {
        return new NextResponse("Employee is required", { status: 400 });
      }
  
      const employee = await prisma.employee.findUnique({
        where: {
          id: parseInt(params.id),
        },
      });
  
      if (!employee) {
        return NextResponse.json("Employee not found!", {status:500})
      }
      return NextResponse.json({message: "Get employee by id successful", employee}, {status:200});
    } catch (err) {
      console.log(err);
      return new NextResponse("error: get employee by id fail", { status: 500 });
    }
  }


  export async function DELETE(req: Request,{ params }: { params: { id: string }}, 
  ) {
    try {
      if (!params.id) {
        return new NextResponse("Employee is required", { status: 400 });
      }
  
      const employee = await prisma.employee.delete({
        where: {
          id: parseInt(params.id),
        },
      });
  
      return NextResponse.json({message: "Delete employee by id successful", employee}, {status:200});
    } catch (err) {
      console.log(err);
      return new NextResponse("error: delete employee by id fail", { status: 500 });
    }
  }


  export async function PUT(req: Request,{ params }: { params: { id: string }}, 
  ) {
    try {
      if (!params.id) {
        return new NextResponse("Employee is required", { status: 400 });
      }
  
      const employeeRequest:EmployeeType = await req.json();
      employeeRequest.birthdate = new Date(employeeRequest.birthdate);

      const employee = await prisma.employee.update(
        {data: employeeRequest,
        where: {id: parseInt(params.id)}});
      
  
      return NextResponse.json({message: "Update employee by id successful", employee}, {status:200});
    } catch (err) {
      console.log(err);
      return new NextResponse("error: update employee by id fail", { status: 500 });
    }
  }