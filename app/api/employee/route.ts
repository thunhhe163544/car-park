import prisma from "@/prisma";
import { EmployeeType } from "@/type/type.dto";
import { NextResponse } from "next/server";



export const GET = async (req: Request, res: NextResponse) => {
    try {
        
        const employees = await prisma.employee.findMany(); //find all employee in db
        return NextResponse.json({ message: "Get all employee successful", employees }, { status: 200 });

    } catch (err) {
        return NextResponse.json({ message: "Error get all employee", err }, { status: 500 });

    } 
 
}


export const POST = async (req: Request, res: NextResponse) => {
    try {
        // const { account, department, address, email, name, phone, password, sex } = await req.json()
        
        const employeeRequest:EmployeeType = await req.json();
        employeeRequest.birthdate = new Date(employeeRequest.birthdate);


        const employee = await prisma.employee.create({data: employeeRequest});
        return NextResponse.json({ message: "Create a employee successful", employee }, { status: 200 });
    } catch (err) {
        console.log(err)
        return NextResponse.json({ message: "Error: Create employee fail", err }, { status: 500 });
    } 
    
}
