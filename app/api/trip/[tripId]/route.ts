import { NextResponse } from "next/server";
import prisma from "../../../../prisma";

    export const GET = async (req:Request, {params}: {params: {tripId: string}}) => {
        
    try {

        if (!params.tripId) {
                  return new NextResponse("Trip is required", { status: 400 });
                }
        const trip = await prisma.trip.findUnique({ where: { id: parseInt(params.tripId) } });
        if (!trip) {
            return NextResponse.json({ message: "Not Found Trip" }, { status: 500 });
        }
        return NextResponse.json({ message: "Get Trip By Id Successful: ", trip }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error: Get Trip By Id FAIL", err }, { status: 500 })
    }

}


export const PUT = async (req:Request, {params}: {params: {tripId: string}}) => {
        
    try {

        if (!params.tripId) {
                  return new NextResponse("Trip is required", { status: 400 });
                }

        const tripRequest = await req.json();
        tripRequest.departureDate = new Date(tripRequest.departureDate);
        tripRequest.departureTime = new Date(tripRequest.departureTime);
        const trip = await prisma.trip.update({ data: tripRequest, where: {id: parseInt(params.tripId)} });
        if (!trip) {
            return NextResponse.json({ message: "Not Found Trip" }, { status: 500 });
        }
        return NextResponse.json({ message: "Update Trip By Id Successful: ", trip }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error: Update Trip By Id FAIL", err }, { status: 500 })
    }

}


export const DELETE = async (req:Request, {params}: {params: {tripId: string}}) => {
        
    try {

        if (!params.tripId) {
                  return new NextResponse("Trip is required", { status: 400 });
                }


        const trip = await prisma.trip.delete({where: {id: parseInt(params.tripId)} });
        if (!trip) {
            return NextResponse.json({ message: "Not Found Trip" }, { status: 500 });
        }
        return NextResponse.json({ message: "Delete Trip By Id Successful: ", trip }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error: Delete Trip By Id FAIL", err }, { status: 500 })
    }

}
