import { NextResponse } from "next/server";
import prisma from "../../../prisma";
import { TripType } from "@/type/type.dto";
import { parse } from 'date-fns';
import { tr } from "date-fns/locale/tr";


export const POST = async (req: Request, res: NextResponse) => {
    try {

        const tripRequest: TripType = await req.json();
        tripRequest.departureDate = new Date(tripRequest.departureDate);


        const trip = await prisma.trip.create({ data: tripRequest });
        return NextResponse.json({ message: "Create a employee successful", trip }, { status: 200 });
    } catch (err) {
        console.log(err)
        return NextResponse.json({ message: "Error: Create employee fail", err }, { status: 500 });
    }

}



export const GET = async (req: Request, res: NextResponse) => {

    try {

        const trips = await prisma.trip.findMany(
            {
                select: {
                    id: true,
                    destination: true,
                    departureTime: true,
                    driver: true,
                    carType: true,
                    bookedTicketNumber: true
                }
            }
        );
        return NextResponse.json({ message: "Get All Trip Successful!", trips }, { status: 200 });

    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Get all trip FAIL", err }, { status: 500 });
    }

}



