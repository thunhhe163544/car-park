import prisma from "@/prisma";
import { BookingOffice } from "@prisma/client";
import { NextResponse } from "next/server";

export const GET = async (req: Request, res: NextResponse) => {

    try {

        const trips = await prisma.trip.findMany({
            distinct: ['destination'],
            select: {
                destination: true,
            }
        });
        const officePlace = await prisma.bookingOffice.findMany({
            select:{
                officePlace:true
            }
        })
        const data = Object.assign({trips},{officePlace});
    return NextResponse.json({ message: "Get All Trip Successful!", data }, { status: 200 });

} catch (err) {
    console.log(err);
    return NextResponse.json({ message: "Get all trip FAIL", err }, { status: 500 });
}
}

export const POST = async (req: Request, res: NextResponse) => {
    try {
        
        const dataRequest:BookingOffice = await req.json();
        dataRequest.endContractDeadline = new Date(dataRequest.endContractDeadline);
        dataRequest.startContractDeadline = new Date(dataRequest.startContractDeadline);
        dataRequest.tripId = 6;


        const bookingOffice = await prisma.bookingOffice.create({data: dataRequest});
        console.log(bookingOffice);
        return NextResponse.json({ message: "Create a Booking Office successful", bookingOffice }, { status: 200 });
    } catch (err) {
        console.log(err)
        return NextResponse.json({ message: "Error: Create Booking Office fail", err }, { status: 500 });
    } 
    
}


