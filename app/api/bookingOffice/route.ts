import prisma from "@/prisma";
import { BookingOffice } from "@/type/type.dto";
import { NextResponse } from "next/server";



export const POST = async (req: Request, res: NextResponse) => {
    try {
        
        const dataRequest:BookingOffice = await req.json();
        dataRequest.endContractDeadline = new Date(dataRequest.endContractDeadline);
        dataRequest.startContractDeadline = new Date(dataRequest.startContractDeadline);

        const bookingOffice = await prisma.bookingOffice.create({data: dataRequest});
        return NextResponse.json({ message: "Create a Booking Office successful", bookingOffice }, { status: 200 });
    } catch (err) {
        console.log(err)
        return NextResponse.json({ message: "Error: Create Booking Office fail", err }, { status: 500 });
    } 
    
}


export const GET = async (req:Request, res: NextResponse) => {
    try {
        const bookingOffices = await prisma.bookingOffice.findMany({
            select: {
                id: true,
                officeName: true,
                trip: {
                    select: {
                        destination: true
                    }
                }
            }
        });
        return NextResponse.json({message: "Get All Booking Office Successful!", bookingOffices}, {status:200});
    } catch (err) {
        console.log(err);
        return NextResponse.json({message: "Get all booking offfice FAIL", err}, {status:500});
    }
}