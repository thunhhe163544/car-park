import { NextResponse } from "next/server";
import prisma from "../../../../prisma";

    export const GET = async (req:Request, {params}: {params: {id: string}}) => {
        
    try {

        if (!params.id) {
                  return new NextResponse("car is required", { status: 400 });
                }
        const car = await prisma.car.findUnique({ where: { licensePlate: (params.id) } });
        if (!car) {
            return NextResponse.json({ message: "Not Found Car" }, { status: 500 });
        }
        return NextResponse.json({ message: "Get Car By Id Successful: ", car }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error: Get car By Id FAIL", err }, { status: 500 })
    }

}


export const PUT = async (req:Request, {params}: {params: {id: string}}) => {
        
    try {

        if (!params.id) {
                  return new NextResponse("car is required", { status: 400 });
                }

        const carRequest = await req.json();
        const car = await prisma.car.update({ data: carRequest, where: {licensePlate: (params.id)} });
        if (!car) {
            return NextResponse.json({ message: "Not Found car" }, { status: 500 });
        }
        return NextResponse.json({ message: "Update car By Id Successful: ", car }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error: Update ca By Id FAIL", err }, { status: 500 })
    }

}


export const DELETE = async (req:Request, {params}: {params: {id: string}}) => {
        
    try {

        if (!params.id) {
                  return new NextResponse("Car is required", { status: 400 });
                }


        const car = await prisma.car.delete({where: {licensePlate: (params.id)} });
        if (!car) {
            return NextResponse.json({ message: "Not Found car" }, { status: 500 });
        }
        return NextResponse.json({ message: "Delete car By Id Successful: ", car }, { status: 200 });
    } catch (err) {
        console.log(err);
        return NextResponse.json({ message: "Error: Delete car By Id FAIL", err }, { status: 500 })
    }

}
