import prisma from "@/prisma";
import { Car } from "@/type/type.dto";
import { NextResponse } from "next/server";

export const GET = async (req: Request, res: NextResponse) => {
    try {
        
        const cars = await prisma.car.findMany(
            {
                select: {
                    licensePlate: true,
                    carType: true,
                    carColor: true,
                    company: true,
                    parkingLot: {
                        select: {
                            parkName: true,
                        }
                    }
                }
            }
        ); //find all employee in db
        return NextResponse.json({ message: "Get all car successful", cars }, { status: 200 });

    } catch (err) {
        return NextResponse.json({ message: "Error get all car", err }, { status: 500 });

    } 
 
}


export const POST = async (req: Request, res: NextResponse) => {
    try {
        
        const carRequest:Car = await req.json();
        carRequest.parkingId = 1;

        const car = await prisma.car.create({data: carRequest});
        return NextResponse.json({ message: "Create a car successful", carRequest }, { status: 200 });
    } catch (err) {
        console.log(err)
        return NextResponse.json({ message: "Error: Create car fail", err }, { status: 500 });
    } 
}
    